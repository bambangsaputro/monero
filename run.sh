#!/bin/bash

if [ ! -f /home/monero/build/xmrig ]; then
    cd /home/monero/build && cmake .. -DWITH_HTTPD=OFF  && make && ./xmrig
else
    ./home/monero/xmrig
fi
